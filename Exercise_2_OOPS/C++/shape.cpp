#include "shape.h"
#include <math.h>

namespace ShapeLibrary {

Point::Point(const double& x,
             const double& y)
{
   _x = x;
   _y = y;
}

Point::Point(const Point& point)
{
    _x = point._x;
    _y = point._y;
}

Ellipse:: Ellipse(const Point& center,
                  const int& a,
                  const int& b)
{
  _a = a;
  _b = b;
}


Circle::Circle(const Point& center,
               const int& radius)
{
   _radius = radius;
}


Triangle::Triangle(const Point& p1,
                   const Point& p2,
                   const Point& p3)
{
  _x1 = p1._x;
  _x2 = p2._x;
  _x3 = p3._x;
  _y1 = p1._y;
  _y2 = p2._y;
  _y3 = p3._y;

}


TriangleEquilateral::TriangleEquilateral(const Point& p1,
                                         const int& edge)
{
   _edge = edge;
}


Quadrilateral::Quadrilateral(const Point& p1,
                             const Point& p2,
                             const Point& p3,
                             const Point& p4)
{
    _x1 = p1._x;
    _x2 = p2._x;
    _x3 = p3._x;
    _x4 = p4._x;
    _y1 = p1._y;
    _y2 = p2._y;
    _y3 = p3._y;
    _y4 = p4._y;

}

Parallelogram::Parallelogram(const Point& p1,
              const Point& p2,
              const Point& p3)
{
   _x1 = p1._x;
   _x2 = p2._x;
   _y1 = p1._y;
   _y3 = p3._y;
}


Rectangle::Rectangle(const Point& p1,
                     const int& base,
                     const int& height)
{
   _base = base;
   _height = height;

}

Square::Square(const Point& p1,
               const int& edge)
{
   _edge = edge;
}



}
