#ifndef SHAPE_H
#define SHAPE_H

#include <iostream>
#include <math.h>

using namespace std;

namespace ShapeLibrary {

  class Point
  {
    public:
      double _x;
      double _y;

    public:
      Point(const double& x,
            const double& y);
      Point(const Point& point);
  };

  class IPolygon {
    public:
      virtual double Area() const = 0;
  };

  class Ellipse : public IPolygon
  {
    private:
      int _a;
      int _b;

    public:
      Ellipse(const Point& center,
              const int& a,
              const int& b);

      double Area() const { return M_PI * _a * _b; }
  };

  class Circle : public IPolygon
  {
    private:
      int _radius;

    public:
      Circle(const Point& center,
             const int& radius);

      double Area() const { return M_PI * pow(_radius, 2); }
  };


  class Triangle : public IPolygon
  {
    private:
      double _x1;
      double _x2;
      double _x3;
      double _y1;
      double _y2;
      double _y3;

    public:
      Triangle(const Point& p1,
               const Point& p2,
               const Point& p3);

      double Area() const
      { return 0.5*abs((_x1*_y2)+(_x2*_y3)+(_x3*_y1)-(_x2*_y1)-(_x3*_y2)-(_x1*_y3)); }
  };


  class TriangleEquilateral : public IPolygon
  {
    private:
      int _edge;

    public:
      TriangleEquilateral(const Point& p1,
                          const int& edge);

      // Diviso 4 = 0.25
      double Area() const { return sqrt(3) * pow(_edge,2) * 0.25; }
  };

  class Quadrilateral : public IPolygon
  {
    private:
      double _x1;
      double _x2;
      double _x3;
      double _y1;
      double _y2;
      double _y3;
      double _x4;
      double _y4;

    public:
      Quadrilateral(const Point& p1,
                    const Point& p2,
                    const Point& p3,
                    const Point& p4);

      double Area() const
      { return 0.5*abs((_x1*_y2)+(_x2*_y3)+(_x3*_y4)+(_x4*_y1)-(_x1*_y4)-(_x2*_y1)-(_x3*_y2)-(_x4*_y3)); }
  };


  class Parallelogram : public IPolygon
  {
    private:
      double _x1;
      double _x2;
      double _y1;
      double _y3;

    public:
      Parallelogram(const Point& p1,
                    const Point& p2,
                    const Point& p3);

      double Area() const
      { return abs((_x1-_x2)*(_y1-_y3)); }
  };

  class Rectangle : public IPolygon
  {
    private:
      int _base;
      int _height;

    public:
      Rectangle(const Point& p1,
                const int& base,
                const int& height);

      double Area() const { return _base * _height; }
  };

  class Square: public IPolygon
  {
    private:
      int _edge;

    public:
      Square(const Point& p1,
             const int& edge);

      double Area() const {return pow(_edge, 2);}
  };
}

#endif // SHAPE_H
