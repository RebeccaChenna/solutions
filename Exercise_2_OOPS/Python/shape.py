import math

class Point:
    def __init__(self, x: float, y: float):
        self.x = x
        self.y = y


class IPolygon:
    def area(self) -> float:
        pass


class Ellipse(IPolygon):
    def __init__(self, center: Point, a: int, b: int):
        self.__a = a
        self.__b = b
    def area(self):
        return math.pi * self.__a * self.__b



class Circle(IPolygon):
    def __init__(self, center: Point, radius: int):
        self.__radius = radius
    def area(self):
        return math.pi * pow(self.__radius,2)


class Triangle(IPolygon):
    def __init__(self, p1: Point, p2: Point, p3: Point):
            self.__x1 = p1.x
            self.__x2 = p2.x
            self.__x3 = p3.x
            self.__y1 = p1.y
            self.__y2 = p2.y
            self.__y3 = p3.y
    def area(self):
        return abs((self.__x1 * self.__y2) + (self.__x2 * self.__y3)
                  + (self.__x3 * self.__y1) - (self.__x1 * self.__y3)
                  - (self.__x2 * self.__y1) - (self.__x3 * self.__y2)) / 2



class TriangleEquilateral(IPolygon):
    def __init__(self, p1: Point, edge: int):
        self.__edge = edge
    def area(self):
        return pow(3,1/2)* pow(self.__edge, 2)/4


class Quadrilateral(IPolygon):
    def __init__(self, p1: Point, p2: Point, p3: Point, p4: Point):
        self.__x1 = p1.x
        self.__x2 = p2.x
        self.__x3 = p3.x
        self.__x4 = p4.x
        self.__y1 = p1.y
        self.__y2 = p2.y
        self.__y3 = p3.y
        self.__y4 = p4.y
    def area(self):
        return abs((self.__x1 * self.__y2)+(self.__x2 * self.__y3)+(self.__x3 * self.__y4)
                   +(self.__x4 * self.__y1)-(self.__x1 * self.__y4)-(self.__x2*self.__y1)
                   -(self.__x3 * self.__y2)-(self.__x4 * self.__y3)) / 2



class Parallelogram(IPolygon):
    def __init__(self, p1: Point, p2: Point, p3: Point):
        self.__x1 = p1.x
        self.__x2 = p2.x
        self.__y1 = p1.y
        self.__y3 = p3.y
    def area(self):
        return abs((self.__x1-self.__x2) * (self.__y1-self.__y3))



class Rectangle(IPolygon):
    def __init__(self, p1: Point, base: int, height: int):
        self.__base = base
        self.__height = height
    def area(self):
        return self.__base * self.__height



class Square(IPolygon):
    def __init__(self, p1: Point, edge: int):
        self.__edge = edge
    def area(self):
        return pow(self.__edge, 2)
