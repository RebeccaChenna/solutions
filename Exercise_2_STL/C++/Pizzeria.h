#ifndef PIZZERIA_H
#define PIZZERIA_H

#include <iostream>
#include <sstream>
#include <vector>
#include <unordered_map>
#include <map>
#include <list>

using namespace std;

namespace PizzeriaLibrary {

  class Ingredient {
    public:
      string Name;
      int Price;
      string Description;
  };

  class Pizza {
    public:
      string Name;
      list<Ingredient> ingredientsPizza;
      void AddIngredient(const Ingredient& ingredient) { ingredientsPizza.push_back(ingredient);}
      int NumIngredients() const { return ingredientsPizza.size(); }
      int ComputePrice() const { int price = 0;
                                 for(list<Ingredient>::const_iterator it = ingredientsPizza.begin(); it!= ingredientsPizza.end(); it++)
                                 {
                                    price += (*it).Price;
                                 }
                                 return price; }
  };

  class Order {
      vector<Pizza> pizzasOrder;
    public:
      void InitializeOrder(int numPizzas) { pizzasOrder.reserve(numPizzas); }
      void AddPizza(const Pizza& pizza) { pizzasOrder.push_back(pizza); }
      const Pizza& GetPizza(const int& position) const { return pizzasOrder[position]; }
      int NumPizzas() const { return pizzasOrder.size(); }
      int ComputeTotal() const { int total = 0;
                               for( unsigned int p = 0; p < pizzasOrder.size(); p++)
                               {
                                 total += pizzasOrder[p].ComputePrice();
                               }
                               return total; }
  };

  class Pizzeria {
    public:
      map<string,Ingredient> _ingredients;
      unordered_map<string,Pizza> _pizzasByName;
      unordered_map<int,Order> _order;

      void AddIngredient(const string& name,
                         const string& description,
                         const int& price);
      const Ingredient& FindIngredient(const string& name) const;
      void AddPizza(const string& name,
                    const vector<string>& ingredients) ;
      const Pizza& FindPizza(const string& name) const ;
      int CreateOrder(const vector<string>& pizzas);
      const Order& FindOrder(const int& numOrder) const;
      string GetReceipt(const int& numOrder) const;
      string ListIngredients() const;
      string Menu() const;
  };
};

#endif // PIZZERIA_H
