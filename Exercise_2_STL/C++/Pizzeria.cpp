#include "Pizzeria.h"

namespace PizzeriaLibrary {

void Pizzeria::AddIngredient(const string &name, const string &description, const int &price)
{
    if(_ingredients.find(name) != _ingredients.end())
        throw runtime_error("Ingredient already inserted");

    _ingredients.insert(pair<string,Ingredient>(name,Ingredient()));
    Ingredient& ingredient = _ingredients[name];
    ingredient.Name = name;
    ingredient.Description = description;
    ingredient.Price = price;
}

const Ingredient &Pizzeria::FindIngredient(const string &name) const
{
    const auto& ingredientIterator = _ingredients.find(name);
    if (ingredientIterator == _ingredients.end() )
        throw runtime_error("Ingredient not found");

    return ingredientIterator->second;
}

string Pizzeria::ListIngredients() const
{
    string listIngredient;
    for ( auto& ingredient : _ingredients)
    {
       Ingredient ingr = ingredient.second;
       listIngredient += ingr.Name + " - '" + ingr.Description + "': " + to_string(ingr.Price) + " euro\n" ;
    }
    return listIngredient;
}


void Pizzeria::AddPizza(const string &name, const vector<string> &ingredients)
{
    if(_pizzasByName.find(name) != _pizzasByName.end())
        throw runtime_error("Pizza already inserted");

    Pizza pizza;
    pizza.Name = name;
    for(unsigned int i = 0; i < ingredients.size(); i++)
    {
        if(_ingredients.find(ingredients[i]) != _ingredients.end())
            pizza.AddIngredient(_ingredients.at(ingredients[i]));
    }
    _pizzasByName.insert(pair<string,Pizza>(name,pizza));
}

const Pizza &Pizzeria::FindPizza(const string &name) const
{
    const auto& pizzaIterator = _pizzasByName.find(name);
    if (pizzaIterator == _pizzasByName.end() )
        throw runtime_error("Pizza not found");

    return pizzaIterator->second;
}

string Pizzeria::Menu() const
{
    string listPizza;
    for( auto& pizzas : _pizzasByName)
    {
        Pizza pizza = pizzas.second;

        listPizza += pizza.Name + " (" + to_string(pizza.NumIngredients()) + " ingredients): " + to_string(pizza.ComputePrice()) + " euro\n";
    }

    return listPizza;
}

int Pizzeria::CreateOrder(const vector<string> &pizzas)
{
    if(pizzas.size() == 0)
        throw runtime_error("Empty order");

     int numOrder = 1000;
     Order order;
     for(unsigned int p = 0; p < pizzas.size(); p++)
     {
         if(_pizzasByName.find(pizzas[p]) != _pizzasByName.end())
             order.AddPizza(_pizzasByName.at(pizzas[p]));
     }
     _order.insert(pair<int,Order>(numOrder,order));
     numOrder++;

    return numOrder - 1;
}

const Order &Pizzeria::FindOrder(const int &numOrder) const
{
    const auto& orderIterator = _order.find(numOrder);
    if (orderIterator == _order.end() )
        throw runtime_error("Order not found");

    return orderIterator->second;
}

string Pizzeria::GetReceipt(const int &numOrder) const
{
    string receipt;
    string total;
    const auto& orderIterator = _order.find(numOrder);
    if (orderIterator == _order.end() )
        throw runtime_error("Order not found");

    total = to_string(orderIterator->second.ComputeTotal());
    unsigned int numPizzas = orderIterator->second.NumPizzas();
    for(unsigned int p = 0; p < numPizzas ; p++)
    {
       Pizza currentPizza;
       currentPizza = orderIterator->second.GetPizza(p);
       receipt += "- " + currentPizza.Name + ", " + to_string(currentPizza.ComputePrice()) + " euro\n";
    }

    return receipt + "  TOTAL: " + total + " euro" + "\n" ;
}


}
