class Ingredient:
    def __init__(self, name: str, price: int, description: str):
        self.Name = name
        self.Price = price
        self.Description = description


class Pizza:
    def __init__(self, name: str):
        self.Name = name
        self.pizzaIngredients = []

    def addIngredient(self, ingredient: Ingredient):
        self.pizzaIngredients.append(ingredient)

    def numIngredients(self) -> int:
        numberIngredients: int = len(self.pizzaIngredients)
        return numberIngredients

    def computePrice(self) -> int:
        price: int = 0
        numberIngredients: int = self.numIngredients()
        for i in range(0, numberIngredients):
            price += self.pizzaIngredients[i].Price
        return price


class Order:
    def __init__(self):
        self.pizzasOrder = []

    def getPizza(self, position: int) -> Pizza:
        return self.pizzasOrder[position]

    def initializeOrder(self, numPizzas: int):
        self.pizzasOrder[numPizzas] = 0

    def addPizza(self, pizza: Pizza):
        self.pizzasOrder.append(pizza)

    def numPizzas(self) -> int:
        numberPizzas: int = len(self.pizzasOrder)
        return numberPizzas

    def computeTotal(self) -> int:
        total: int = 0
        numberPizzas: int = self.numPizzas()
        for p in range(0, numberPizzas):
            tot: Pizza = self.pizzasOrder[p]
            total += tot.computePrice()
        return total


class Pizzeria:
    def __init__(self):
        self.ingredients = {}
        self.pizzasByName = {}
        self.order = {}

    def addIngredient(self, name: str, description: str, price: int):
        if name in self.ingredients:
            raise Exception("Ingredient already inserted")

        self.ingredients[name] = Ingredient(name, price, description)

    def findIngredient(self, name: str) -> Ingredient:
        if name not in self.ingredients:
            raise Exception("Ingredient not found")

        return self.ingredients[name]

    def addPizza(self, name: str, ingredients: []):
        if name in self.pizzasByName:
            raise Exception("Pizza already inserted")

        pizza = Pizza(name)
        pizza.Name = name
        numIngr: int = len(ingredients)
        for i in range(0, numIngr):
            if ingredients[i] in self.ingredients:
                pizza.addIngredient(self.ingredients[ingredients[i]])

        self.pizzasByName[name] = pizza

    def findPizza(self, name: str) -> Pizza:
        if name not in self.pizzasByName:
            raise Exception("Pizza not found")

        return self.pizzasByName[name]

    def createOrder(self, pizzas: []) -> int:
        numPizzas: int = len(pizzas)
        if (numPizzas == 0):
            raise Exception("Empty order")

        numOrder: int = 1000
        ord = Order()
        for p in range(0, numPizzas):
            if pizzas[p] in self.pizzasByName:
                ord.addPizza(self.pizzasByName[pizzas[p]])

        self.order[numOrder] = ord
        numOrder = numOrder + 1

        return numOrder - 1

    def findOrder(self, numOrder: int) -> Order:
        if numOrder not in self.order:
            raise Exception("Order not found")

        return self.order[numOrder]

    def getReceipt(self, numOrder: int) -> str:
        receipt: str = ''
        if numOrder not in self.order:
            raise Exception("Order not found")

        orderFind: Order = self.order[numOrder]
        total = str(orderFind.computeTotal())
        numPizzas: int = orderFind.numPizzas()
        for p in range(0, numPizzas):
            currentPizza: Pizza = orderFind.getPizza(p)
            receipt += "- " + currentPizza.Name + ", " + str(currentPizza.computePrice()) + " euro\n"

        return receipt + "  TOTAL: " + total + " euro" + "\n"

    def listIngredients(self) -> str:
        sortedIngredient = sorted(self.ingredients)
        listIngr: str = ''
        numIngredients: int = len(self.ingredients)
        for i in range(0, numIngredients):
            ingr = self.ingredients[sortedIngredient[i]]
            listIngr += ingr.Name + " - '" + ingr.Description + "': " + str(ingr.Price) + " euro\n"

        return listIngr

    def menu(self) -> str:
        listPizzas: str = ''

        pizzaName = sorted(self.pizzasByName)
        numPizzas = len(self.pizzasByName)
        for p in range(0, numPizzas):
            pizza = self.pizzasByName[pizzaName[p]]
            listPizzas += pizza.Name + " (" + str(pizza.numIngredients()) + " ingredients): " + str(
                pizza.computePrice()) + " euro\n"

        return listPizzas
