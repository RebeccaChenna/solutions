#include "Intersector2D1D.h"

// ***************************************************************************
Intersector2D1D::Intersector2D1D()
{
    toleranceParallelism = 1.0E-7;
    toleranceIntersection = 1.0E-7;
}
Intersector2D1D::~Intersector2D1D()
{

}

// ***************************************************************************
void Intersector2D1D::SetPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
  planeTranslationPointer = &planeTranslation;
  planeNormalPointer = &planeNormal;
}
// ***************************************************************************
void Intersector2D1D::SetLine(const Vector3d& lineOrigin, const Vector3d& lineTangent)
{
  lineOriginPointer = &lineOrigin;
  lineTangentPointer = &lineTangent;
}
// ***************************************************************************
bool Intersector2D1D::ComputeIntersection()
{
  if ( (*planeNormalPointer).transpose() * (*lineTangentPointer) >= toleranceParallelism )
  {
      double first = (*planeNormalPointer * (*planeTranslationPointer) ).squaredNorm();
      double second = (*planeNormalPointer).transpose() * (*lineOriginPointer);

      intersectionParametricCoordinate = (first - second) / ((*planeNormalPointer).transpose() * (*lineTangentPointer));
      intersectionType = PointIntersection;
  }

  else
  {
      if ( (*planeNormalPointer).transpose() * (*lineOriginPointer) <= toleranceIntersection )
          intersectionType = Coplanar;

      else
          intersectionType = NoInteresection;


      return false;
  }

  return true;
}
