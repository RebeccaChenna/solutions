#include "Intersector2D2D.h"

// ***************************************************************************
Intersector2D2D::Intersector2D2D()
{
    toleranceParallelism = 1.0E-7;
    toleranceIntersection = 1.0E-7;
}

Intersector2D2D::~Intersector2D2D()
{

}
// ***************************************************************************
void Intersector2D2D::SetFirstPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
    matrixNomalVector.row(0) = planeNormal;
    rightHandSide(0) = planeTranslation;

    return;
}

// ***************************************************************************
void Intersector2D2D::SetSecondPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
    matrixNomalVector.row(1) = planeNormal;
    rightHandSide(1) = planeTranslation;
    return ;
}

// ***************************************************************************
bool Intersector2D2D::ComputeIntersection()
{
    Vector3d N1 = Vector3d(matrixNomalVector(0,0),matrixNomalVector(0,1),matrixNomalVector(0,2));
    Vector3d N2 = Vector3d(matrixNomalVector(1,0),matrixNomalVector(1,1),matrixNomalVector(1,2));
    tangentLine = N1.cross(N2);

    double check = ( (matrixNomalVector.row(0).transpose())*rightHandSide(0) -
                     (matrixNomalVector.row(1).transpose())*rightHandSide(1) ).squaredNorm();

    if(tangentLine.squaredNorm() <= ToleranceParallelism())
    {
        if(abs(check) <= toleranceIntersection)
            intersectionType = Coplanar;

        else
            intersectionType = NoInteresection;

        return false;
    }
    else
    {
        intersectionType = LineIntersection;
        return true;
    }

}
